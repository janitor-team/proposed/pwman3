Source: pwman3
Section: utils
Priority: optional
Maintainer: Emmanuel Bouthenot <kolter@debian.org>
Build-Depends:
    debhelper (>= 13),
    debhelper-compat (= 13),
    dh-python,
    python3-all (>= 2.6.6-3~),
    python3-setuptools,
    python3-cryptography,
    po-debconf
Rules-Requires-Root: no
Standards-Version: 4.6.0.1
Homepage: https://github.com/pwman3/pwman3
Vcs-Git: https://salsa.debian.org/kolter/pwman3.git
Vcs-Browser: https://salsa.debian.org/kolter/pwman3
X-Python-Version: >= 3.6

Package: pwman3
Architecture: all
Pre-Depends:
    debconf
Depends:
    ${misc:Depends},
    ${python3:Depends},
    python3-pkg-resources,
    python3-pycryptodome,
    python3-colorama (>= 0.2.4)
Recommends: python3-mysqldb, python3-psycopg2
Suggests: default-mysql-server | postgresql, xsel
Description: console password management application
 Pwman3 aims to provide a simple but powerful command line interface for
 password management. It allows one to store your password in a sqlite database
 locked by a master password which can be encrypted with different algorithms
 (e.g Blowfish, AES, DES3).
 .
 It is also possible to tag them to ease searches in a large amount of
 passwords
